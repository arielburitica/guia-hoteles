$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 2000
    });
    $('#reservar').on('show.bs.modal', function(e){
        console.log('El form modal se muestra');
        $('.btn-reserva').removeClass('btn-primary');
        $('.btn-reserva').addClass('btn-default');
        $('.btn-reserva').prop('disabled',true);
    });
    $('#reservar').on('shown.bs.modal', function(e){
        console.log('El form modal se ha mostrado');
    });
    $('#reservar').on('hide.bs.modal', function(e){
        console.log('El form modal se oculta');
    });
    $('#reservar').on('hidden.bs.modal', function(e){
        console.log('El form modal se ha ocultado');
        $('.btn-reserva').removeClass('btn-default');
        $('.btn-reserva').addClass('btn-primary');
        $('.btn-reserva').prop('disabled',false);
    });
});